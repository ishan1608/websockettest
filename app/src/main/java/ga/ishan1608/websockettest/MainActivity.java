package ga.ishan1608.websockettest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.neovisionaries.ws.client.WebSocket;
import com.neovisionaries.ws.client.WebSocketAdapter;
import com.neovisionaries.ws.client.WebSocketException;
import com.neovisionaries.ws.client.WebSocketExtension;
import com.neovisionaries.ws.client.WebSocketFactory;
import com.neovisionaries.ws.client.WebSocketFrame;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final String WEBSOCKET_ENDPOINT = "wss://ishanatmuz.pagekite.me/wsapp/";
    private static final String AUTHORIZATION_HEADER_VALUE = "ApiKey ishan:f08025c1d11795431b210219fc3e85bf70837209";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final TextView wsTextView = (TextView) findViewById(R.id.websocket_messages_text_view);
        try
        {
            // Create a WebSocketFactory instance.
            WebSocketFactory factory = new WebSocketFactory();

            // Create a WebSocket. The scheme part can be one of the following:
            // 'ws', 'wss', 'http' and 'https' (case-insensitive). The user info
            // part, if any, is interpreted as expected. If a raw socket failed
            // to be created, an IOException is thrown.
            WebSocket ws = factory.createSocket(WEBSOCKET_ENDPOINT);

            // Setting Authorization header
            ws.addHeader("authorization", AUTHORIZATION_HEADER_VALUE);
            // Adding websocket extensions
            ws.addExtension(new WebSocketExtension("permessage-deflate"));
            ws.addExtension(new WebSocketExtension("client_max_window_bits"));

            // Register a listener to receive WebSocket events.
            ws.addListener(new WebSocketAdapter() {
                @Override
                public void onConnected(WebSocket websocket, Map<String, List<String>> headers) throws Exception {
                    super.onConnected(websocket, headers);
                    // Send a text frame.
                    Log.d(TAG, "Sending a text frame.");
                    websocket.sendText("Hello From Android.");

                    // Send a ping frame.
                    Log.d(TAG, "Send a ping frame.");
                    websocket.sendPing("Are you there?");
                }

                @Override
                public void onTextMessage(WebSocket websocket, final String message) throws Exception {
                    // Received a text message.
                    Log.d(TAG, "Received message " + message);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            wsTextView.append("\n" + message);
                        }
                    });
                }

                @Override
                public void onBinaryFrame(WebSocket websocket, WebSocketFrame frame) throws Exception {
                    super.onBinaryFrame(websocket, frame);
                    Log.d(TAG, "onBinaryFrame received");
                    Log.d(TAG, frame.getPayloadText());
                }

                @Override
                public void onCloseFrame(WebSocket websocket, WebSocketFrame frame) throws Exception {
                    super.onCloseFrame(websocket, frame);
                    Log.d(TAG, "onCloseFrame received");
                    Log.d(TAG, frame.getPayloadText());
                }

                @Override
                public void onContinuationFrame(WebSocket websocket, WebSocketFrame frame) throws Exception {
                    super.onContinuationFrame(websocket, frame);
                    Log.d(TAG, "onContinuationFrame received");
                    Log.d(TAG, frame.getPayloadText());
                }

                @Override
                public void onFrame(WebSocket websocket, WebSocketFrame frame) throws Exception {
                    super.onFrame(websocket, frame);
                    Log.d(TAG, "onFrame received");
                    Log.d(TAG, new String(frame.getPayload()));
                    Log.d(TAG, frame.getPayloadText());
                }

                @Override
                public void onFrameError(WebSocket websocket, WebSocketException cause, WebSocketFrame frame) throws Exception {
                    super.onFrameError(websocket, cause, frame);
                    Log.e(TAG, "onFrameError received");
                    Log.e(TAG, frame.getPayloadText());
                    Log.e(TAG, cause.getMessage());
                }

                @Override
                public void onPingFrame(WebSocket websocket, WebSocketFrame frame) throws Exception {
                    super.onPingFrame(websocket, frame);
                    Log.d(TAG, "onPingFrame received");
                    Log.d(TAG, frame.getPayloadText());
                }

                @Override
                public void onPongFrame(WebSocket websocket, WebSocketFrame frame) throws Exception {
                    super.onPongFrame(websocket, frame);
                    Log.d(TAG, "onPongFrame received");
                    Log.d(TAG, frame.getPayloadText());
                }

                @Override
                public void onTextFrame(WebSocket websocket, WebSocketFrame frame) throws Exception {
                    super.onTextFrame(websocket, frame);
                    Log.d(TAG, "onTextFrame received");
                    Log.d(TAG, frame.getPayloadText());
                }

                @Override
                public void onConnectError(WebSocket websocket, WebSocketException exception) throws Exception {
                    super.onConnectError(websocket, exception);
                    Log.e(TAG, "onConnectError after calling connect");
                }
            });

            ws.connectAsynchronously();


        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, "Failed to create a raw socket");
        }
    }
}
